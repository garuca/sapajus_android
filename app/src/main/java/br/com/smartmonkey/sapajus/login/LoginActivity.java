package br.com.smartmonkey.sapajus.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import br.com.smartmonkey.sapajus.R;
import br.com.smartmonkey.sapajus.register.RegisterActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements Animation.AnimationListener{
    @BindView(R.id.imageView)
    ImageView imageView;

    @BindView(R.id.textInputLayoutEmail)
    TextInputLayout textInputLayoutEmail;

    @BindView(R.id.textInputLayoutPassword)
    TextInputLayout textInputLayoutPassword;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.textViewForgetPassword)
    TextView textViewForgetPassword;

    @BindView(R.id.textViewRegister)
    TextView textViewRegister;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initialize();
    }
    private void initialize() {
        startAnimation();
        overridePendingTransition(0, 0);
    }
    private void startAnimation(){
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move);
        animation.setFillAfter(true);
        animation.setAnimationListener(this);
        imageView.startAnimation(animation);

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        textInputLayoutEmail.setVisibility(View.VISIBLE);
        textInputLayoutPassword.setVisibility(View.VISIBLE);
        textViewForgetPassword.setVisibility(View.VISIBLE);
        textViewRegister.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.actionDone) {
            textInputLayoutEmail.setVisibility(View.GONE);
            textInputLayoutPassword.setVisibility(View.GONE);
            textViewForgetPassword.setVisibility(View.GONE);
            textViewRegister.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void register(View view){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }
}
