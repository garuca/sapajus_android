package br.com.smartmonkey.sapajus.start;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import br.com.smartmonkey.sapajus.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class StartActivity extends AppCompatActivity implements Animation.AnimationListener{
    @BindView(R.id.imageView)
    ImageView imageView;


    @BindView(R.id.textInputLayoutEmail)
    TextInputLayout textInputLayoutEmail;

    @BindView(R.id.textInputLayoutPassword)
    TextInputLayout textInputLayoutPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);
        initialize();

    }
    private void initialize() {
      startAnimation();

    }
    private void startAnimation(){
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move);
        animation.setFillAfter(true);
        animation.setAnimationListener(this);
        imageView.startAnimation(animation);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        textInputLayoutEmail.setVisibility(View.VISIBLE);
        textInputLayoutPassword.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
